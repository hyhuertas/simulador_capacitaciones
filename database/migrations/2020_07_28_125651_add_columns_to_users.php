<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('numero_documento')->unique()->after('password');
            $table->integer('codigo_usercrm')->nullable()->after('numero_documento');
            $table->smallInteger('rol_user_id')->nullable()->after('codigo_usercrm');
            $table->boolean('activo')->default(1)->after('rol_user_id');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('codigo_usercrm');
            $table->dropColumn('rol_user_id');
            $table->dropColumn('numero_documento');
            $table->dropColumn('activo');
        });
    }
}
