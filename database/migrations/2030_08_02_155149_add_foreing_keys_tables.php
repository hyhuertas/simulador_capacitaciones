<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeingKeysTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('capacitaciones_hojas', function($table) {
            $table->foreign('capacitacion_id')
            ->references('id')->on('capacitaciones')
            ->onDelete('NO ACTION');
            $table->foreign('hoja_id')
            ->references('id')->on('hojas')
            ->onDelete('NO ACTION');
        });

        Schema::table('elementos', function($table) {
            $table->foreign('hoja_id')
            ->references('id')->on('hojas')
            ->onDelete('NO ACTION');
        });

        Schema::table('items', function($table) {
            $table->foreign('elemento_id')
            ->references('id')->on('elementos')
            ->onDelete('NO ACTION');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('capacitaciones_hojas', function($table) {
        
            $table->dropForeign('capacitaciones_hojas_capacitacion_id_foreign');
            $table->dropForeign('capacitaciones_hojas_hoja_id_foreign');
        });

        Schema::table('elementos', function($table) {
            $table->dropForeign('elementos_hoja_id_foreign');
        });

        Schema::table('items', function($table) {
            $table->dropForeign('items_elemento_id_foreign');
        });


    }
}
