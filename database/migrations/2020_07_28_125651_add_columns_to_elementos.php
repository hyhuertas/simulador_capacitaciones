<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToElementos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('elementos', function (Blueprint $table) {
            $table->string('theme')->comment("vue => vuetify; nativo => nativo")->default("vue")->after('type');
            $table->longText('style')->comment("Aplica para cambios de estilo en nativo, (HTML)")->nullable()->after('theme');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elementos', function (Blueprint $table) {
            $table->dropColumn('tema');
            $table->dropColumn('style');
        });
    }
}
