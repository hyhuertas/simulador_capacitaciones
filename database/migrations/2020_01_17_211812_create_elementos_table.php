<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elementos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('accion')->comment("1 => SIGUIENTE; 2 => ANTERIOR")->nullable();
            $table->string('type');
            $table->string('width');
            $table->string('height');
            $table->string('x');
            $table->string('y');
            $table->string('widthPx');
            $table->string('heightPx');
            $table->string('label');
            $table->string('fontsize')->nullable();
            $table->string('formattext')->nullable();
            $table->string('formattextalign')->nullable();
            $table->string('placeholder')->nullable();
            $table->boolean('rounded')->nullable()->default(false);
            $table->string('color')->nullable();
            $table->boolean('outlined')->nullable()->default(false);
            $table->unsignedBigInteger('hoja_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elementos');
    }
}
