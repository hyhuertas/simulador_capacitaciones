<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('roles')->delete();

        \DB::table('roles')->insert(array (
            0 =>
            array (
                'idrol' => 1,
                'descripcion' => 'Admin',
                'rol' => '1',
                'created_at' => '2020-01-24 13:46:13',
                'updated_at' => '2020-01-24 13:46:15',
            )
        ));


    }
}
