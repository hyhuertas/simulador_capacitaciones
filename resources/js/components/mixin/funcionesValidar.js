export default {
    data() {
        return {
            rules_form: {
                requerido: v => !!v || "Este campo es requerido",
                //!/^([0-9])*$/
                numeros: v => /^([0-9])*$/.test(v) || "Este campo debe ser númerico",
                alfanumerico: v =>
                    /^([ 0-9A-Za-zñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ])*$/.test(v) ||
                    "Este campo no permite caracteres especiales",
                sololetras: v =>
                    /^([ A-Za-zñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ])*$/.test(
                        v
                    ) || "Este campo no permite números ni caracteres especiales",
                maximo255caracteres: v =>
                    (v && v.length <= 255) ||
                    "Este campo no debe superar los 255 caracteres",
                maximo10caracterestel: v =>
                    (v && v.length <= 10) ||
                    "Este campo no debe superar los 10 caracteres",

                email: v => /.+@.+\..+/.test(v) || "Correo electrónico no válido",

                numerospass: v => /^(?=.*\d)/.test(v) || 'Debe contener al menos un valor númerico',
                letraspass: v => /^(?=.*[a-z])(?=.*[A-Z])/.test(v) || 'Debe contener letras mayusculas y minusculas',
                minpass: v => /^.{6,15}$/.test(v) || 'Debe tener entre 6 y 15 caracteres'
            },
        }
    },
}
