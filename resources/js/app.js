/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import VueDraggableResizable from 'vue-draggable-resizable'


// optionally import default styles
import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
import VueTour from 'vue-tour'
 
require('vue-tour/dist/vue-tour.css')

window.Vue = require('vue');
Vue.component('vue-draggable-resizable', VueDraggableResizable)
Vue.use(VueTour)


Vue.use(Vuetify)
Vue.use(VueRouter)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('view-component', require('./components/ViewComponent.vue').default);
Vue.component('preview-component', require('./components/PreviewComponent.vue').default);
Vue.component('crearnuevo-component', require('./components/CrearNuevoComponent.vue').default);
Vue.component('editarform-component', require('./components/editarFormComponent.vue').default);
Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('tablacapacitaciones-component', require('./components/TablaCapacitacionesComponent.vue').default);


let crearnuevo_component = {
    template: `<crearnuevo-component></crearnuevo-component>`
}
let editarform_component = {
    template: `<editarform-component></editarform-component>`
}

let home_component = {
    template: `<home-component></home-component>`
}

let preview_component = {
    template: `<preview-component></preview-component>`
}

let view_component = {
    template: `<view-component></view-component>`
}

let tablacapacitaciones_component = {
    template: `<tablacapacitaciones-component></tablacapacitaciones-component>`
}


const router = new VueRouter({
    routes: [{
        path: '/home',
        name: 'Inicio',
        component: home_component,
    },{
        path: '/crearnuevo',
        name: 'Crear Capacitación',
        component: crearnuevo_component,
    },{
        path: '/editar-capacitacion/:capacitacion_id',
        name: 'Editar Capacitación',
        component: editarform_component,
    },{
        path: '/preview',
        name: 'preview',
        component: preview_component,
    },{
        path: '/view/:capacitacion_id',
        name: 'view',
        component: view_component,
        props: true
    },{
        path: '/tablacapacitaciones',
        name: 'tablacapacitaciones',
        component: tablacapacitaciones_component
    }
    // {
    //     path: '/reporte',
    //     name: 'Reporte gestiones',
    //     component: reporte_component,
    // }
    ],
    mode: 'history'
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    vuetify: new Vuetify({
        iconfont: 'mdi',
        theme: {
            themes: {
                light: {
                    primary: '#1B506F',
                    secondary: '#6606D2',
                    accent: '#82B1FF',
                    error: '#FF5252',
                    info: '#2196F3',
                    success: '#4CAF50',
                    warning: '#FFC107',
                }
            }
        },
    }),
    router,
    el: '#app',
});
