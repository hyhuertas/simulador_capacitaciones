<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Item extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;


    protected $table = "items";
    protected $fillable = [
        'nombre', 'elemento_id'
    ];
}
