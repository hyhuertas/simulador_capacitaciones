<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Elemento extends Model implements Auditable
{   
    
    use \OwenIt\Auditing\Auditable;


    protected $table="elementos";
    protected $fillable=[
        'accion', 'type' ,'theme', 'style', 'width', 'height', 'x', 'y', 'widthPx', 'heightPx', 'label', 'fontsize', 'formattext', 'formattextalign', 'placeholder', 'rounded', 'color', 'outlined', 'hoja_id'
    ];


    public function hoja(){
        return $this->belongsTo('App\Models\Hoja', 'hoja_id', 'id');
    }

    public function itemsSelect(){
        return $this->hasMany('App\Models\Item', 'elemento_id', 'id');
    }

}
