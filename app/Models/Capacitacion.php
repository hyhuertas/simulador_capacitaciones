<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Capacitacion extends Model implements Auditable
{
     use \OwenIt\Auditing\Auditable;

    protected $table="capacitaciones";
    protected $fillable=[
        'nombre', 'user_id'
    ];

    public function contarHojas(){
        return $this->belongsToMany('App\Models\Hoja','capacitaciones_hojas','capacitacion_id','hoja_id');
    }
    public function hojas(){
        return $this->belongsToMany('App\Models\Hoja','capacitaciones_hojas','capacitacion_id','hoja_id');
    }


}
