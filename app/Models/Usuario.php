<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $timestamps = false;
    // protected $connection = 'master_connection';
    protected $table = 'crm_masterclaro.usuarios';
    protected $fillable = ['id_usuario','nombre_usuario', 'apellido_usuario', 'usuario', 'password','estado','tipoUsuario'];
    protected $appends = ['codigo_usercrm','nombre'];
    public function getCodigoUsercrmAttribute()
    {
        
        return  $this->id_usuario;
    }
    public function getNombreAttribute()
    {
        
        return  $this->nombre_usuario.' '.$this->apellido_usuario;
    }
   
}
