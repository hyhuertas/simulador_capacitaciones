<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Hoja extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;


    protected $table = "hojas";
    protected $fillable = [
        'nombre', 'imagen_url', 'imagen_nombre'
    ];

    public function capacitaciones()
    {
        return $this->belongsToMany('App\Models\Capacitacion', 'capacitaciones_hojas', 'hoja_id', 'capacitacion_id');
    }

    public function elementos()
    {
        return $this->hasMany('App\Models\Elemento', 'hoja_id', 'id');
    }
}
