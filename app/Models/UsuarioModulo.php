<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioModulo extends Model
{
    // protected $connection = 'MASTERCLARO';
    // protected $table = 'crm_masterclaro.permisos_modulos_usuarios';
    protected $table = 'crm_masterclaro.roles';
    protected $fillable = ['id_rol','descripcion_rol', 'id_usuario', 'id_modulo', 'numero_rol'];
    protected $primaryKey = 'id_usuario';

}
