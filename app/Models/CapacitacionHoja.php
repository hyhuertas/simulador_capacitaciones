<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class CapacitacionHoja extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;


    protected $table="capacitaciones_hojas";
    protected $fillable=[
        'capacitacion_id', 'hoja_id'
    ];

}
