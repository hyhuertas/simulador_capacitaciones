<?php

namespace App\Http\Controllers;

use App\Models\Capacitacion;
use App\Models\CapacitacionHoja;
use App\Models\Elemento;
use App\Models\Hoja;
use App\Models\Item;
use Illuminate\Http\Request;
use Session;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class CapacitacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $inicio = -1;
        if ($request->rowsPerPage && $request->page) {
            $inicio = ($request->page - 1) * $request->rowsPerPage;
        }

        $capacitacion['total_registros'] = Capacitacion::count();

        $capacitacion['data_registros'] = Capacitacion::withCount('hojas');
        if ($request->search) {
            $capacitacion['data_registros']  = $capacitacion['data_registros']->where('nombre', 'like', '%' . $request->search . '%');
        }
        $capacitacion['data_registros']  = $capacitacion['data_registros']->limit($request->rowsPerPage)
            ->offset($inicio)->orderBy('created_at', 'DESC')
            ->get();

        return $capacitacion;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $user_id =  Session::get('id_usuario_simulador'); //id usuario log
        DB::beginTransaction();
        $formData = $request->all();
        $nombrehoja = $formData['nombrehoja'];
        $controlForm = $formData['controlForm'];
        $uploadImg = $formData['uploadImg'];
        $titulotablero = $formData['titulotablero'];


        $rules = [
            'titulotablero' => 'required|unique:capacitaciones,nombre|min:3|max:150',
            'nombrehoja.*' => 'required|min:3|max:150',
            'uploadImg' => 'required',
            'uploadImg.*' => 'image|mimes:jpeg,png,jpg|max:4096'
        ];


        $messages = [
            'titulotablero.unique' => 'Ya existe el título de la capacitación.',
            'titulotablero.required' => 'Debes agregar un título a la capacitación.',
            'titulotablero.min' => 'El título debe tener minimo :min caracteres.',
            'titulotablero.max' => 'El título debe tener maximo :max caracteres.',
            'nombrehoja.*.required' => 'Debes asignar un nombre al tema.',
            'nombrehoja.*.min' => 'El nombre de los temas debe tener minimo :min caracteres.',
            'nombrehoja.*.max' => 'El nombre de los temas debe tener maximo :max caracteres.',
            'uploadImg.required' => 'Los temas deben tener una imagen de fondo adjunta.',
            'uploadImg.*.image' => 'Los temas deben tener una imagen de fondo adjunta.',
            'uploadImg.*.mimes' => 'Los formatos validos son jpg y png.',
        ];


        $this->validate($request, $rules, $messages);



        try {

            $insertCapacitacion = Capacitacion::create([
                'nombre' => $titulotablero,
                'user_id' => $user_id,
            ]);


            for ($i = 0; $i < count($nombrehoja); $i++) {


                $nombre_hoja = $nombrehoja[$i];
                $bg_upload = $uploadImg[$i];

                $controles = json_decode($controlForm[$i], true);


                if ($request->hasfile('uploadImg')) {
                    $files = $request->file('uploadImg');

                    $filename = $bg_upload->getClientOriginalName(); //NOMBRE ORIGINAL
                    $fileName = pathinfo($filename, PATHINFO_FILENAME); //quita la extension
                    $extension = $bg_upload->getClientOriginalExtension(); // EXTENSION DEL ARCHIVO
                    $newFile = $fileName . Str::random(6) . '.' . $extension;
                    $urlFile = $insertCapacitacion->id . '/' . $newFile;
                    $path = Storage::putFileAs(
                        'public/upload_files/bgfondos/' . $insertCapacitacion->id,
                        $bg_upload,
                        $newFile
                    );



                    $insertHoja = Hoja::create([
                        'nombre' => $nombre_hoja,
                        'imagen_url' => $urlFile,
                        'imagen_nombre' => $newFile
                    ]);

                    $insertCapacitacionHoja = CapacitacionHoja::create([
                        'capacitacion_id' => $insertCapacitacion->id,
                        'hoja_id' => $insertHoja->id
                    ]);

                    foreach ($controles as $key => $value) {

                        if (!$value['delete']) {

                            $propiedades = $value['propiedades'];
                            $tipo = $value['type'];
                            $alto = $value['height'];
                            $ancho = $value['width'];
                            $altoPx = $value['heightPx'];
                            $anchoPx = $value['widthPx'];
                            $x = $value['x'];
                            $y = $value['y'];
                            $theme = $value['theme'];
                            $label = $propiedades['label'];
                            $hoja_id = $insertHoja->id;

                            if ($tipo == "imagen") {
                                //IMAGEN DE FONDO
                                $insertElemento = Elemento::create([
                                    'type' => $tipo,
                                    'theme' => $theme,
                                    'height' => $alto,
                                    'width' => $ancho,
                                    'x' => $x,
                                    'y' => $y,
                                    'heightPx' => $altoPx,
                                    'widthPx' => $anchoPx,
                                    'label' => $label,
                                    'hoja_id' => $hoja_id
                                ]);
                            } else {
                                //CUALQUIER OTRO ELEMENTO



                                $formattext = null;
                                $fontsize = null;
                                $formattextalign = null;
                                $placeholder = null;
                                $rounded = null;
                                $color = null;
                                $outlined = null;
                                $accion = null;

                                if (array_key_exists('formatText', $propiedades)) {
                                    $formattext = implode(" ", $propiedades['formatText']);
                                }
                                if (array_key_exists('fontSize', $propiedades)) {
                                    $fontsize = $propiedades['fontSize'];
                                }
                                if (array_key_exists('formatTextAlign', $propiedades)) {
                                    $formattextalign = $propiedades['formatTextAlign'];
                                }
                                if (array_key_exists('placeholder', $propiedades)) {
                                    $placeholder = $propiedades['placeholder'];
                                }
                                if (array_key_exists('rounded', $propiedades)) {
                                    $rounded = $propiedades['rounded'];
                                }
                                if (array_key_exists('color', $propiedades)) {
                                    $color = $propiedades['color'];
                                }
                                if (array_key_exists('outlined', $propiedades)) {
                                    $outlined = $propiedades['outlined'];
                                }

                                if (array_key_exists('accion', $propiedades)) {
                                    $accion = $propiedades['accion'];
                                }

                                if (strlen($label) > 150) {
                                    return response()->json([
                                        'errors' => [
                                            'label' => ['El limite de caracteres para los elementos agregados es de 150.']
                                        ],
                                        'message' => 'The given data was invalid.', //sample message
                                    ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                }

                                if ($tipo == "comentario" || $tipo == "texto") {
                                    $text = "";
                                    if ($tipo == "comentario") {
                                        $text = 'EL comentario agregado no puede estar vacío, valide la información.';
                                    } else if ($tipo == "texto") {
                                        $text = 'EL parrafo agregado no puede estar vacío, valide la información.';
                                    }
                                    if (strlen($label) == 0) {
                                        return response()->json([
                                            'errors' => [
                                                'label' => [$text]
                                            ],
                                            'message' => 'The given data was invalid.', //sample message
                                        ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                    }
                                }

                                $insertElemento = Elemento::create([
                                    'accion' => $accion,
                                    'type' => $tipo,
                                    'theme' => $theme,
                                    'height' => $alto,
                                    'width' => $ancho,
                                    'x' => $x,
                                    'y' => $y,
                                    'heightPx' => $altoPx,
                                    'widthPx' => $anchoPx,
                                    'label' => $label,
                                    'hoja_id' => $hoja_id,
                                    'formattext' => $formattext,
                                    'fontsize' => $fontsize,
                                    'formattextalign' => $formattextalign,
                                    'placeholder' => $placeholder,
                                    'rounded' => $rounded,
                                    'outlined' => $outlined,
                                    'color' => $color
                                ]);
                                if ($tipo == "select") {
                                    if (array_key_exists('items', $propiedades)) {
                                        $items = $propiedades['items'];

                                        foreach ($items as $key => $value) {


                                            if (strlen($value['valor']) > 150) {
                                                return response()->json([
                                                    'errors' => [
                                                        'label' => ['El limite de caracteres para los elementos agregados es de 150.']
                                                    ],
                                                    'message' => 'The given data was invalid.', //sample message
                                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                            }

                                            if (strlen($value['valor']) == 0) {
                                                return response()->json([
                                                    'errors' => [
                                                        'label' => ['El valor de los items agregados a los select no puede estar vacío, valide la información.']
                                                    ],
                                                    'message' => 'The given data was invalid.', //sample message
                                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                            }

                                            $insertItem = Item::create([
                                                'nombre' => $value['valor'],
                                                'elemento_id' => $insertElemento->id
                                            ]);
                                        }
                                    }
                                }
                            }
                            // dd($controles);
                        }
                    }
                }
            }

            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];



            DB::commit();

            return $data_return;
        } catch (\Exception $e) {



            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
            ];

            return $data_return;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $capacitacion = Capacitacion::where('id', $id)
            ->with('hojas.elementos.itemsSelect')
            ->first();

        return $capacitacion;
    }
    public function infoCapacitacionEditar($id)
    {
        $dataEditar['titulo_capacitacion'] = null;
        $dataEditar['temas_capacitacion'] = [];
        $capacitacion = Capacitacion::where('id', $id)
            ->with('hojas.elementos.itemsSelect')
            ->first();

        if (isset($capacitacion)) {
            $dataEditar['titulo_capacitacion'] = $capacitacion->nombre;
            foreach ($capacitacion->hojas as $keyTema => $temas) {
                $dataEditar['temas_capacitacion'][$keyTema]['nombrehoja'] = $temas->nombre;
                $dataEditar['temas_capacitacion'][$keyTema]['imagen_url'] = "/storage/upload_files/bgfondos/" . $temas->imagen_url;
                $dataEditar['temas_capacitacion'][$keyTema]['imagen'] =  "/storage/upload_files/bgfondos/" . $temas->imagen_url;
                $dataEditar['temas_capacitacion'][$keyTema]['addBg'] = true;
                $dataEditar['temas_capacitacion'][$keyTema]['uploadImg'] = null;
                $dataEditar['temas_capacitacion'][$keyTema]['tema_id_bd'] = $temas->id;
                $dataEditar['temas_capacitacion'][$keyTema]['controlForm'] = [];

                foreach ($temas->elementos as $keyControl => $control) {

                    $propiedades = [];

                    $minWidth = 100;
                    $minHeight = 100;


                    $widthFondo = (float) $temas->elementos[0]->widthPx;
                    $heightFondo = (float) $temas->elementos[0]->heightPx;
                    // $width = 15;
                    // $height = 11;
                    // $heightPx = 70;
                    // $widthPx = 0; //TAMAÑO DE LA PANTALLA


                    $width = (float) $control->width;
                    $height = (float) $control->height;
                    $widthPx = (float) $control->widthPx;
                    $heightPx = (float) $control->heightPx;

                    if ($control->type == 'comentario') {

                        $minWidth = 29;
                        $minHeight = 29;

                        // $width = $control->width;
                        // $height = $control->height;
                        // $widthPx = $control->widthPx;
                        $propiedades = [
                            "color" => $control->color,
                            "fontSize" => $control->fontsize,
                            "label" => $control->label,
                            "formatTextAlign" => $control->formattextalign,
                            "formatText" => explode(" ",  $control->formattext) //ARRAY
                        ];
                    }
                    if ($control->type == 'imagen') {
                        // $width = 100;
                        $height = 100;
                        // $heightPx = 600;
                        $propiedades = [
                            "label" => $control->label
                        ];
                    }
                    if ($control->type == 'texto') {
                        $minWidth = 112;
                        $minHeight = 27;
                        // $height = 10;
                        // $widthPx = 202; 
                        $propiedades = [
                            "color" => $control->color,
                            "label" => $control->label,
                            "formatText" => explode(" ",  $control->formattext), //ARRAY,
                            "formatTextAlign" => $control->formattextalign,
                            "fontSize" => $control->fontsize
                        ];
                    }
                    if ($control->type == 'campotexto') {
                        $minWidth = 202;
                        $minHeight = 70;
                        // $widthPx = 202; 
                        $propiedades = [
                            "color" => $control->color,
                            "label" => $control->label,
                            "rounded" => (bool) $control->rounded,
                            "placeholder" => $control->placeholder
                        ];
                    }
                    if ($control->type == 'button') {
                        $minWidth = 100;
                        $minHeight = 30;
                        // $widthPx = 202;
                        // $heightPx = 50;
                        $propiedades = [
                            "label" => $control->label,
                            "rounded" => (bool) $control->rounded,
                            "outlined" => (bool) $control->outlined,
                            "color" => $control->color,
                            "accion" => $control->accion
                        ];
                    }
                    if ($control->type == 'select') {
                        $items = [];
                        foreach ($control->itemsSelect as $key => $value) {
                            $items[] = [
                                "valor" => $value->nombre
                            ];
                        }
                        // dd($control->itemsSelect);
                        $minWidth = 202;
                        $minHeight = 70;
                        // $widthPx = 202;
                        $propiedades = [
                            "color" => $control->color,
                            "label" => $control->label,
                            "items" => $items,
                            "rounded" => (bool) $control->rounded,
                            "placeholder" => $control->placeholder
                        ];
                    }

                    $dataEditar['temas_capacitacion'][$keyTema]['controlForm'][$keyControl] = [
                        "modificado" =>  false,
                        "control_id_bd" =>  $control->id,
                        "minWidth" =>  $minWidth,
                        "minHeight" =>  $minHeight,
                        "width" =>  $width,
                        "height" =>  $height,
                        "propiedades" =>  $propiedades,
                        "type" =>  $control->type,
                        "theme" =>  $control->theme,
                        "widthPx" =>  $widthPx,
                        "heightPx" =>  $heightPx,
                        "delete" =>  false,
                        "lock" =>  false,
                        "x" => ((float) $control->x),
                        "y" => ((float) $control->y),
                        "x_px" => ((float) $control->x * 1251) / 100,
                        "y_px" => ((float) $control->y * 600) / 100
                    ];
                }
            }
        }

        return $dataEditar;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { }
    public function actualizarCapacitacion(Request $request, $id)
    {


        $user_id =  Session::get('id_usuario_simulador'); //id usuario log
        DB::beginTransaction();
        $formData = $request->all();
        $nombrehoja = $formData['nombrehoja'];
        $controlForm = $formData['controlForm'];
        $uploadImg = $formData['uploadImg'];
        $titulotablero = $formData['titulotablero'];
        $tema_id_bd = $formData['tema_id_bd'];


        $rules = [
            'titulotablero' => 'required|unique:capacitaciones,nombre,' . $id . ',id|min:3|max:150',
            'nombrehoja.*' => 'required|min:3|max:150',
            'imagen.*' => 'required',
            // 'uploadImg.*' => 'image|mimes:jpeg,png,jpg|max:4096'
        ];


        $messages = [
            'titulotablero.unique' => 'Ya existe el título de la capacitación.',
            'titulotablero.required' => 'Debes agregar un título a la capacitación.',
            'titulotablero.min' => 'El título debe tener minimo :min caracteres.',
            'titulotablero.max' => 'El título debe tener maximo :max caracteres.',
            'nombrehoja.*.required' => 'Debes asignar un nombre al tema.',
            'nombrehoja.*.min' => 'El nombre de los temas debe tener minimo :min caracteres.',
            'nombrehoja.*.max' => 'El nombre de los temas debe tener maximo :max caracteres.',
            'imagen.*.required' => 'Los temas deben tener una imagen de fondo adjunta.',
            // 'uploadImg.*.image' => 'Los temas deben tener una imagen de fondo adjunta.',
            // 'uploadImg.*.mimes' => 'Los formatos validos son jpg y png.',
        ];


        $this->validate($request, $rules, $messages);



        try {

            $insertCapacitacion = Capacitacion::where('id', $id)->update([
                'nombre' => $titulotablero,
                'user_id' => $user_id,
            ]);

            // dd($request->all());

            //ELIMINAR HOJAS Y AGREGAR LAS NUEVAS CREADAS PARA LA CAPACITACION

            $idHojasDelete = [];

            $buscarHojasEliminar = CapacitacionHoja::where('capacitacion_id', $id)
                ->WhereNotIn('hoja_id', $tema_id_bd)
                ->get();

            $idHojasDelete = $buscarHojasEliminar->pluck('hoja_id');

            $eliminarHojasRelacion = CapacitacionHoja::where('capacitacion_id', $id)
                ->WhereIn('hoja_id', $idHojasDelete)
                ->delete();

            $eliminarElementos = Elemento::WhereIn('hoja_id', $idHojasDelete)
                ->delete();

            $eliminarHojas = Hoja::WhereIn('id', $idHojasDelete)
                ->delete();

            // dd($idHojasDelete);

            //ELIMINAR HOJAS Y AGREGAR LAS NUEVAS CREADAS PARA LA CAPACITACION



            for ($i = 0; $i < count($nombrehoja); $i++) {
// dd($uploadImg);

                $nombre_hoja = $nombrehoja[$i];
                $bg_upload = $uploadImg[$i];
                $hoja_id = $tema_id_bd[$i];


                $controles = json_decode($controlForm[$i], true);

                if ($hoja_id > 0) {

                    // dd($hoja_id);
                    
                    //ID DE HOJA, ACTUALIZACION DE HOJA O TEMA

                    if ($request->hasfile('uploadImg.'.$i)) {
                        // dd($hoja_id);
                        //IMAGEN ADJUNTA O CAMBIO DE IMAGEN
                        $files = $request->file('uploadImg.'.$i);

                        // dd($bg_upload->getClientOriginalName());

                        $filename = $bg_upload->getClientOriginalName(); //NOMBRE ORIGINAL
                        $fileName = pathinfo($filename, PATHINFO_FILENAME); //quita la extension
                        $extension = $bg_upload->getClientOriginalExtension(); // EXTENSION DEL ARCHIVO
                        $newFile = $fileName . Str::random(6) . '.' . $extension;
                        $urlFile = $id . '/' . $newFile;
                        $path = Storage::putFileAs(
                            'public/upload_files/bgfondos/' . $id,
                            $bg_upload,
                            $newFile
                        );
                        // dd($filename);

                        $insertHoja = Hoja::where('id', $hoja_id)->update([
                            'nombre' => $nombre_hoja,
                            'imagen_url' => $urlFile,
                            'imagen_nombre' => $newFile
                        ]);
                    } else {
                        //SOLO ACTUALIZAR TITULO

                        $insertHoja = Hoja::where('id', $hoja_id)->update([
                            'nombre' => $nombre_hoja,
                            // 'imagen_url' => $urlFile,
                            // 'imagen_nombre' => $newFile
                        ]);
                    }
                    //ELIMINAR ELEMENTOS

                    $selectElementos = Elemento::Where('hoja_id', $hoja_id)
                        ->Where('type', 'select')
                        ->get();

                    $idsSlects = [];

                    $idsSlects = $selectElementos->pluck('id');

                    if (count($idsSlects)) {
                        $eliminarElementos = Item::WhereIn('elemento_id', $idsSlects)
                            ->delete();
                    }


                    $eliminarElementos = Elemento::Where('hoja_id', $hoja_id)
                        ->delete();
                } else {
                    //NUEVA HOJA CREAR NUEVA HOJA

                    if ($request->hasfile('uploadImg.'.$i)) {
                        //IMAGEN ADJUNTA O CAMBIO DE IMAGEN
                        $files = $request->file('uploadImg.'.$i);

                        $filename = $bg_upload->getClientOriginalName(); //NOMBRE ORIGINAL
                        $fileName = pathinfo($filename, PATHINFO_FILENAME); //quita la extension
                        $extension = $bg_upload->getClientOriginalExtension(); // EXTENSION DEL ARCHIVO
                        $newFile = $fileName . Str::random(6) . '.' . $extension;
                        $urlFile = $id . '/' . $newFile;
                        $path = Storage::putFileAs(
                            'public/upload_files/bgfondos/' . $id,
                            $bg_upload,
                            $newFile
                        );



                        $insertHoja = Hoja::create([
                            'nombre' => $nombre_hoja,
                            'imagen_url' => $urlFile,
                            'imagen_nombre' => $newFile
                        ]);

                        $insertCapacitacionHoja = CapacitacionHoja::create([
                            'capacitacion_id' => $id,
                            'hoja_id' => $insertHoja->id
                        ]);


                        $hoja_id = $insertHoja->id;
                    }
                }


                foreach ($controles as $key => $value) {

                    if (!$value['delete']) {

                        $propiedades = $value['propiedades'];
                        $tipo = $value['type'];
                        $theme = $value['theme'];
                        $alto = $value['height'];
                        $ancho = $value['width'];
                        $altoPx = $value['heightPx'];
                        $anchoPx = $value['widthPx'];
                        $x = $value['x'];
                        $y = $value['y'];
                        $label = $propiedades['label'];


                        if ($tipo == "imagen") {
                            //IMAGEN DE FONDO
                            $insertElemento = Elemento::create([
                                'type' => $tipo,
                                'theme' => $theme,
                                'height' => $alto,
                                'width' => $ancho,
                                'x' => $x,
                                'y' => $y,
                                'heightPx' => $altoPx,
                                'widthPx' => $anchoPx,
                                'label' => $label,
                                'hoja_id' => $hoja_id
                            ]);
                        } else {
                            //CUALQUIER OTRO ELEMENTO



                            $formattext = null;
                            $fontsize = null;
                            $formattextalign = null;
                            $placeholder = null;
                            $rounded = null;
                            $color = null;
                            $outlined = null;
                            $accion = null;

                            if (array_key_exists('formatText', $propiedades)) {
                                $formattext = implode(" ", $propiedades['formatText']);
                            }
                            if (array_key_exists('fontSize', $propiedades)) {
                                $fontsize = $propiedades['fontSize'];
                            }
                            if (array_key_exists('formatTextAlign', $propiedades)) {
                                $formattextalign = $propiedades['formatTextAlign'];
                            }
                            if (array_key_exists('placeholder', $propiedades)) {
                                $placeholder = $propiedades['placeholder'];
                            }
                            if (array_key_exists('rounded', $propiedades)) {
                                $rounded = $propiedades['rounded'];
                            }
                            if (array_key_exists('color', $propiedades)) {
                                $color = $propiedades['color'];
                            }
                            if (array_key_exists('outlined', $propiedades)) {
                                $outlined = $propiedades['outlined'];
                            }

                            if (array_key_exists('accion', $propiedades)) {
                                $accion = $propiedades['accion'];
                            }

                            if (strlen($label) > 150) {
                                return response()->json([
                                    'errors' => [
                                        'label' => ['El limite de caracteres para los elementos agregados es de 150.']
                                    ],
                                    'message' => 'The given data was invalid.', //sample message
                                ], Response::HTTP_UNPROCESSABLE_ENTITY);
                            }

                            if ($tipo == "comentario" || $tipo == "texto") {
                                $text = "";
                                if ($tipo == "comentario") {
                                    $text = 'EL comentario agregado no puede estar vacío, valide la información.';
                                } else if ($tipo == "texto") {
                                    $text = 'EL parrafo agregado no puede estar vacío, valide la información.';
                                }
                                if (strlen($label) == 0) {
                                    return response()->json([
                                        'errors' => [
                                            'label' => [$text]
                                        ],
                                        'message' => 'The given data was invalid.', //sample message
                                    ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                }
                            }

                            $insertElemento = Elemento::create([
                                'accion' => $accion,
                                'type' => $tipo,
                                'theme' => $theme,
                                'height' => $alto,
                                'width' => $ancho,
                                'x' => $x,
                                'y' => $y,
                                'heightPx' => $altoPx,
                                'widthPx' => $anchoPx,
                                'label' => $label,
                                'hoja_id' => $hoja_id,
                                'formattext' => $formattext,
                                'fontsize' => $fontsize,
                                'formattextalign' => $formattextalign,
                                'placeholder' => $placeholder,
                                'rounded' => $rounded,
                                'outlined' => $outlined,
                                'color' => $color
                            ]);
                            if ($tipo == "select") {
                                if (array_key_exists('items', $propiedades)) {
                                    $items = $propiedades['items'];

                                    foreach ($items as $key => $value) {


                                        if (strlen($value['valor']) > 150) {
                                            return response()->json([
                                                'errors' => [
                                                    'label' => ['El limite de caracteres para los elementos agregados es de 150.']
                                                ],
                                                'message' => 'The given data was invalid.', //sample message
                                            ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                        }

                                        if (strlen($value['valor']) == 0) {
                                            return response()->json([
                                                'errors' => [
                                                    'label' => ['El valor de los items agregados a los select no puede estar vacío, valide la información.']
                                                ],
                                                'message' => 'The given data was invalid.', //sample message
                                            ], Response::HTTP_UNPROCESSABLE_ENTITY);
                                        }

                                        $insertItem = Item::create([
                                            'nombre' => $value['valor'],
                                            'elemento_id' => $insertElemento->id
                                        ]);
                                    }
                                }
                            }
                        }
                        // dd($controles);
                    }
                }

            }

            // dd($hoja_id);
            // dd('fuera');
            $data_return['respuesta'] = [
                'codigo' => 202,
                'icon' => 'mdi-check-circle',
                'color' => 'success',
                'text' => 'Información almacenada correctamente.',
            ];



            DB::commit();

            return $data_return;
        } catch (\Exception $e) {



            DB::rollback();
            $data_return['respuesta'] = [
                'codigo' => 404,
                'icon' => 'mdi-alert-octagon',
                'color' => 'error',
                'text' => $e->getMessage(),
                'linea' => $e->getLine(),
            ];

            return $data_return;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
