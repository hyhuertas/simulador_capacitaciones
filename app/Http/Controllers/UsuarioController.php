<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use App\Models\UsuarioModulo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class UsuarioController extends Controller
{
    public function validarAppmaster(Request $request)
    {


        
        try {
            if (!isset($_GET['crm']) || !isset($_GET['idusuario'])) {
                return abort('404');
            } else {
                $crm = $_GET['crm'];
                $usuario_id = $_GET['idusuario'];


                $rol = UsuarioModulo::where('id_usuario', '=', $usuario_id)
                    ->where('id_modulo', '=', $crm)
                    ->first();



                if (isset($rol)) {
                    $usu = Usuario::where('id_usuario', '=', $usuario_id)
                        ->where('estado', '=', 'Habilitado')
                        ->first();

                    if (isset($usu)) {
                        $user = User::where('codigo_usercrm', $rol->id_usuario)->first();
                    } else {
                        abort('404');
                    }
                } else {
                    abort('404');
                }

                if (!isset($user)) { //si no existe el usuario
                    $user = new User(); //se define una nueva instancia
                    $user->password = Hash::make(123456);
                }

                //se actualiza o se crea el nuevo usuario
                $user->name = $usu->nombre_usuario . ' ' . $usu->apellido_usuario;
                $user->numero_documento = $usu->cedula_usuario;
                $user->codigo_usercrm = $usu->id_usuario;
                $user->rol_user_id = $rol->numero_rol;
                $user->save();

                // Auth::login($user, true);

                $rolusu = $rol->numero_rol;
                $idusuario = $usuario_id;
                $crm = $_GET['crm'] ?? session('crm');
                //se crean variable de sesion para redirigir a la vista de usuario
                session(['rolusu' => $rolusu]);
                session(['idusuario' => $idusuario]);
                session(['crm' => $crm]);


                Session::put('id_usuario_simulador',$idusuario);
                // Session::put('usuario_simulador', $consulta_usuario->usuario);
                Session::put('nom_completo_simulador',  $user->nombre);
                Session::put('numero_rol_simulador', $rolusu);




                if ($rolusu == "1") {
                    return redirect('/home');
                }
                if ($rolusu == "2") {
                    
                }
                if ($rolusu == "3") {
                  
                }
            }
        } catch (DecryptException $e) {
            abort('404');
        }
        



            // $consulta_usuario = UsuarioModulo::where('id_grupo', $request->crm)
            //     ->where('id_usuario', $request->idusuario)
            //     ->first();


            // if (is_null($consulta_usuario)) {
            //     return redirect('/404');
            // } else {
            //     Session::put('id_usuario_simulador', $consulta_usuario->id_usuario);
            //     Session::put('usuario_simulador', $consulta_usuario->usuario);
            //     Session::put('nom_completo_simulador', $consulta_usuario->nombre);
            //     Session::put('numero_rol_simulador', $consulta_usuario->numero_rol);

            //     if ($consulta_usuario->numero_rol == 1) {
            //         return redirect('/home');
            //     }
                
            // }


    }

}
