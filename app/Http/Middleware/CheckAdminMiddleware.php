<?php

namespace App\Http\Middleware;

use Closure;
use Session;



class CheckAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(empty(Session::get('numero_rol_simulador'))){
            return redirect('/404');
        }

        return $next($request);
    }
}
