<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsuarioController@validarAppmaster');
Route::resource('crudCapacitaciones', 'CapacitacionesController');
Route::get('infoCapacitacionEditar/{capacitacion_id}', 'CapacitacionesController@infoCapacitacionEditar');
Route::post('actualizarCapacitacion/{capacitacion_id}', 'CapacitacionesController@actualizarCapacitacion');


Route::group(['middleware' => ['roleAdmin']], function () {


    Route::get('/home', function () {
        return view('welcome');
    });

    Route::get('/crearnuevo', function () {
        return view('welcome');
    });
    Route::get('/editar-capacitacion/{capacitacion_id}', function () {
        return view('welcome');
    });
    Route::get('/preview', function () {
        return view('welcome');
    });
    Route::get('/tablacapacitaciones', function () {
        return view('welcome');
    });
});


Route::get('/view/{capacitacion_id}', function () {
    return view('welcome');
});

